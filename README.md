# slider-component

#### 自定义进度条条组件

使用 movable-area + movable-view + css实现可拖动的进度条，支持自定义颜色、宽度、步长等属性值。

- 属性

|属性名|默认值|必须|类型|说明|
|--|--|--|--|--|
|sliderAreaBg|#E9EDF5|false|String|滑动条的背景颜色|
|sliderStartColor|#537DFC|false|String|滑动条开始颜色|
|sliderEndColor|#5DBEF2|false|String|滑动条结束颜色|
|sliderButtonColor|#ffffff|false|String|滑动托柄颜色|
|partsCount|5|false|Integer|定义滑动条分成几部分，类似于设置步长的效果|

- 事件

|事件|参数|返回值|说明|
|--|--|--|--|
|sliding|object|-|参数与 movable-view 的@change一致，监听滑动条的滑动事件|
|getClientInfoBySelector|-|-|获取滑动条的宽度信息; 为了适配不同页面长度，调用该函数会自动适配宽度|
|getSliderWidth|-|Integer|获取当前进度条移动距离，单位：px|
|getSliderCurrentPart|-|Integer|获取当前滑动到那个区域，根据 partsCount 的值表示第几个区域，如：第一个区域返回0|
|setSliderValue|Integer|-|设置滑动条的值，传入的值表示滑动条的像素长度(px)|
|setSliderValueByPart|Integer|-|根据 partsCount 平分滑动条的长度，通过 part 直接设置滑动条的长度|
|setSliderValueByPercentage|Integer|-|根据百分值设置进度条的值|


#### 使用方式
需要通过 ref 调用组件`setSliderValue`、`setSliderValueByPart`、`setSliderValueByPercentage`这三个函数之中的任意一个函数设置进度条的值